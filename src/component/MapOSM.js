import React, { useState, useContext } from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { MapContext } from "provider/MapProvider";

import PointInfo from "component/PointInfo";
const MapOSM = ({ style, center, zoom, ...props }) => {
  const {
    activePoint,
    variableChoiced,
    findActivePoint,
    setActivePoint,
  } = useContext(MapContext);

  const handleClick = (e) => {
    console.log("lat et long", e);
    findActivePoint(e.latlng.lat, e.latlng.lng);
  };
  return (
    <Map
      style={style}
      center={center}
      zoom={zoom}
      onClick={(e) => {
        handleClick(e);
      }}
    >
      {activePoint && (
        <Popup
          position={activePoint.position}
          onClose={() => {
            setActivePoint(null);
          }}
          maxWidth="auto"
        >
          {activePoint.type === "info" && <PointInfo />}
          {activePoint.type === "warn" && <p>{activePoint.msg}</p>}
        </Popup>
      )}
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
    </Map>
  );
};

export default MapOSM;
