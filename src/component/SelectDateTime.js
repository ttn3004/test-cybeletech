import React, { useState, useContext } from 'react';
import { InputGroup } from 'react-bootstrap';
import { MapContext } from 'provider/MapProvider';
import 'react-datepicker/dist/react-datepicker.css';
import { registerLocale } from 'react-datepicker';
import DatePicker from 'react-datepicker';
import fr from 'date-fns/locale/fr';
registerLocale('fr', fr);

const SelectDateTime = ({ ...props }) => {
  const { dateChoiced, setDateChoiced } = useContext(MapContext);
  const handleChange = (date) => {
    setDateChoiced(date);
  };
  return (
    <InputGroup className=" mr-sm-2" style={{ zIndex: '9999' }}>
      <InputGroup.Prepend>
        <InputGroup.Text id="basic-addon1">&#128197;</InputGroup.Text>
      </InputGroup.Prepend>
      <DatePicker
        selected={dateChoiced}
        className="form-control"
        onChange={(e) => {
          handleChange(e);
        }}
      />
    </InputGroup>
  );
};

export default SelectDateTime;
