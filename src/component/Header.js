import React, { useState, useContext } from 'react';
import { Navbar, Form, InputGroup, FormControl, Button } from 'react-bootstrap';
import SelectDateTime from 'component/SelectDateTime';
import SelectVariable from 'component/SelectVariable';
import { MapContext } from 'provider/MapProvider';

const Header = ({ ...props }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <Navbar className="bg-light justify-content-between" fixed="top">
      <Form
        inline
        onSubmit={(e) => {
          handleSubmit(e);
        }}
      >
        <SelectDateTime />
        <SelectVariable />
      </Form>
    </Navbar>
  );
};

export default Header;
