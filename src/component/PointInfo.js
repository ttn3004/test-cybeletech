import React, { useContext, useMemo } from 'react';
import { FormControl, Button } from 'react-bootstrap';
import { MapContext } from 'provider/MapProvider';
import DatePicker from 'react-datepicker';
import LineChartOSM from 'component/chart/LineChartOSM';
import { optionSelectVariable } from 'config/appConfig';
const PointInfo = ({ ...props }) => {
  const { variableChoiced, dateChoiced } = useContext(MapContext);
  const labelVariableChoiced = useMemo(() => {
    if (variableChoiced) {
      for (let i = 0; i < optionSelectVariable.length; i++) {
        const obj = optionSelectVariable[i];
        if (obj.id === variableChoiced) {
          return obj.value;
        }
      }
    }
  }, [variableChoiced]);

  console.log(labelVariableChoiced);
  return (
    <React.Fragment>
      {/*   <h1>{dateChoiced}</h1> */}
      <DatePicker selected={dateChoiced} readOnly={true} />
      <span className="h6 mx-2">{labelVariableChoiced}</span>
      <LineChartOSM />
    </React.Fragment>
  );
};

export default PointInfo;
