import React, { useContext } from 'react';
import { MapContext } from 'provider/MapProvider';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ReferenceLine,
} from 'recharts';

const LineChartOSM = ({ ...props }) => {
  const { activePoint } = useContext(MapContext);
  console.log('activePoint', activePoint);
  return (
    <React.Fragment>
      {activePoint.yTicks && (
        <LineChart
          width={600}
          height={300}
          data={activePoint.dataForChart}
          margin={{
            top: 10,
            right: 10,
            left: 10,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="date" />
          <YAxis dataKey="value" ticks={activePoint.yTicks} />
          <Tooltip />
          <Legend />
          <ReferenceLine
            x={activePoint.refDataX}
            stroke="red"
            label="Valeur courante"
          />
          <ReferenceLine
            y={activePoint.refDataY}
            stroke="red"
          />
          <Line
            type="monotone"
            dataKey="value"
            stroke="#8884d8"
            activeDot={{ r: 8 }}
          />
        </LineChart>
      )}
    </React.Fragment>
  );
};

export default LineChartOSM;
