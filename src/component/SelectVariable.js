import React, { useContext } from 'react';
import { Navbar, Form, InputGroup, FormControl, Button } from 'react-bootstrap';
import { MapContext } from 'provider/MapProvider';
import { optionSelectVariable } from 'config/appConfig';
const SelectVariable = ({ ...props }) => {
  const { variableChoiced, setVariableChoiced, setActivePoint } = useContext(
    MapContext
  );
  const handleChange = (e) => {
    setVariableChoiced(e.target.value);
    setActivePoint(null);
  };
  return (
    <InputGroup className=" mr-sm-2">
      <InputGroup.Prepend>
        <InputGroup.Text id="basic-addon1">⛅</InputGroup.Text>
      </InputGroup.Prepend>
      <Form.Control
        as="select"
        onChange={(e) => {
          handleChange(e);
        }}
      >
        <option hidden>Sélectionner une variable</option>
        {optionSelectVariable.map((e) => {
          return (
            <option key={e.id} value={e.id}>
              {e.value}
            </option>
          );
        })}
      </Form.Control>
    </InputGroup>
  );
};

export default SelectVariable;
