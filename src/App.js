import React, { useEffect } from 'react';
import MapOSM from 'component/MapOSM';
import Header from 'component/Header';
import {
  map_style,
  map_center_position,
  map_center_zoom,
} from 'config/appConfig';
import 'App.css';
const App = () => {
  useEffect(() => {
    console.log('App');
  }, []);
  return (
    <div className="container">
      <Header />
      <MapOSM
        style={map_style}
        center={map_center_position}
        zoom={map_center_zoom}
      />
    </div>
  );
};

export default App;
