export const map_style = {
  width: '100%',
  height: '100vh',
};

export const map_center_position = {
  //France by default
  lat: 46.71109,
  lng: 1.7191036,
};
export const map_center_zoom = 7;
export const optionSelectVariable = [
  {
    id: 't_2m:C',
    value: 'Température à 2m',
  },
  {
    id: 'relative_humidity_2m:p',
    value: 'Humidité relative',
  },
  {
    id: 'dew_point_2m:C',
    value: 'Point de rosé à 2m',
  },
  {
    id: 'solar_power:MW',
    value: 'Irradiance globale',
  },
];
export const map_local_url = 'http://localhost:3001/query';
export const FORMAT_DATE = 'YYYY-MM-DD';
export const FORMAT_DATE_CHART = 'MM-DD';