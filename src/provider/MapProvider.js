import React, { useState } from "react";
import axios from "axios";
import moment from "moment";
import {
  FORMAT_DATE,
  FORMAT_DATE_CHART,
  map_local_url,
} from "config/appConfig";
export const MapContext = React.createContext();
const MapProvider = (props) => {
  const [dateChoiced, setDateChoiced] = useState(new Date());
  const [variableChoiced, setVariableChoiced] = useState("");
  const [lat, setLat] = useState();
  const [lon, setLon] = useState();
  const [activePoint, setActivePoint] = useState("");
  const [warn, setWarn] = useState(null);

  const findActivePoint = async (eLat, eLon) => {
    setLat(eLat);
    setLon(eLon);
    const obj = {
      position: [eLat, eLon],
    };
    if (!variableChoiced) {
      obj.msg = "Veuillez choisir une date et une variable";
      obj.type = "warn";
      setActivePoint(obj);
    } else {
      if (eLat && eLon && variableChoiced && dateChoiced) {
        const nextDate = moment(dateChoiced, FORMAT_DATE).add(1, "days");
        const param = {
          parameter: variableChoiced,
          "coordinates.lat": Math.floor(eLat),
          "coordinates.lon": Math.floor(eLon),
          "coordinates.dates.date": {
            $gte: moment(dateChoiced).format(FORMAT_DATE),
            $lt: moment(nextDate).format(FORMAT_DATE),
          },
        };
        /* 
        
        'coordinates.lon': eLon,
        'coordinates.dates.date': moment(dateChoiced).format(FORMAT_DATE),
         */
        const res = await axios.post(map_local_url, param);

        if (res && res.data) {
          const {
            dataForChart,
            xTicks,
            yTicks,
            refChoiced,
          } = constructDataForChart(res.data);

          obj.data = res.data;
          obj.dataForChart = dataForChart;
          obj.refDataX = refChoiced.date;
          obj.refDataY = refChoiced.value;
          obj.xTicks = xTicks;
          obj.yTicks = yTicks;
          obj.type = "info";

          setActivePoint(obj);
        }else{
          obj.msg = "Le point choisi ne dispose pas de donnée.";
          obj.type = "warn";
          setActivePoint(obj);
        }
      }
    }
  };

  const constructDataForChart = (json) => {
    // console.log(json);
    const dataForChart = [];
    const xTicks = [];
    const yTicks = [];
    const refChoiced = {};
    json.coordinates[0].dates.map((e, key) => {
      const dt = moment(e.date).format(FORMAT_DATE_CHART);
      const valNumber = Number(e.value);
      const obj = {};
      obj.date = dt;
      obj.value = e.value;
      dataForChart.push(obj);
      yTicks.push(valNumber);
      xTicks.push(dt);
      if (
        dt.toString() ===
        moment(dateChoiced).format(FORMAT_DATE_CHART).toString()
      ) {
        refChoiced.date = dt;
        refChoiced.value = valNumber;
      }
    });
    return { dataForChart, xTicks, yTicks, refChoiced };
  };
  return (
    <MapContext.Provider
      value={{
        dateChoiced,
        setDateChoiced,
        variableChoiced,
        setVariableChoiced,
        lat,
        setLat,
        lon,
        setLon,
        activePoint,
        setActivePoint,
        findActivePoint,
        warn,
        setWarn,
      }}
    >
      {props.children}
    </MapContext.Provider>
  );
};
export default MapProvider;
